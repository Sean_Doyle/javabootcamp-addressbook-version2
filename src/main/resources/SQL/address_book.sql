-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2016 at 02:41 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `address_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `address_book_entries`
--

CREATE TABLE `address_book_entries` (
  `Name` varchar(256) NOT NULL,
  `Address` varchar(256) NOT NULL,
  `PhoneNumber` varchar(10) NOT NULL,
  `Email` varchar(256) NOT NULL,
  `ZipCode` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `address_book_entries`
--

INSERT INTO `address_book_entries` (`Name`, `Address`, `PhoneNumber`, `Email`, `ZipCode`) VALUES
('Foremane Mike', 'Kings Lane', '2931023', 'forManMike@live.ie', '291'),
('Bloggs Joe', 'Test Data Center 3, Foo Bar', '0868008135', 'joe-bloggs@version1.com', '054'),
('Bloggs Joesphine', 'Test Data Center 2, Foo Bar', '0871337585', 'joesphine-bloggs@version1.com', '034'),
('Doe John', 'Marys Cross', '8649863251', 'johndoe@gmail.com', '543'),
('Doyle Sean', '123 MH', '0863550817', 'Sean.Doyle@version1.com', '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `address_book_entries`
--
ALTER TABLE `address_book_entries`
  ADD PRIMARY KEY (`Email`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
