package app;

import java.util.Comparator;

import app.dto.AddressBookEntry;

public interface NameSortComparator {
	Comparator<AddressBookEntry> abeNameSort = new Comparator<AddressBookEntry>(){		
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Name = abe1.getName().toLowerCase();
			String abe2Name = abe2.getName().toLowerCase();
			
			return abe1Name.compareTo(abe2Name);
		}
	};
}
