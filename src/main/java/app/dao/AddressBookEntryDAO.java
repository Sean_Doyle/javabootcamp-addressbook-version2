package app.dao;

import java.util.ArrayList;

import app.dto.AddressBookEntry;

public interface AddressBookEntryDAO {		
	//Return Type					Method							Parameters
	String							createAddressBookEntry			(AddressBookEntry abe);
	AddressBookEntry 				getAddressBookEntry				(String email);
	ArrayList<AddressBookEntry> 	getAllAddressBookEntries		();
	String 							updateAddressBookEntry			(AddressBookEntry abeOld, AddressBookEntry abeNew);
	void 							deleteAddressBookEntry			(String email);
}