package app.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import app.dto.AddressBookEntry;

@Repository
public class AddressBookEntryDAOImpl implements AddressBookEntryDAO {
	ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
	private JdbcTemplate jdbcTemplate = new JdbcTemplate((DataSource) context.getBean("dataSource"));	
	private ArrayList<AddressBookEntry> addressBook = new ArrayList<>();
		

	@Transactional
	public String createAddressBookEntry(AddressBookEntry abe) {
		if(entryInCache(abe.getEmail())) return "\n Email already in use."; 
		
		String sql = "INSERT INTO address_book_entries (Name, Address, PhoneNumber, Email, ZipCode) VALUES "
				+ "(\'" + abe.getName() + "\',"
				+ " \'" + abe.getAddress() + "\',"
				+ " \'" + abe.getPhoneNum() + "\',"
				+ " \'" + abe.getEmail() + "\',"
				+ " \'" + abe.getZipCode() + "\')";
		
		jdbcTemplate.execute(sql);
		addToCache(abe);
		return "\n User successfully created!";
	}

	@Transactional
	public AddressBookEntry getAddressBookEntry(String email) {
		if(entryInCache(email))	return getCachedEntry(email);
		
		String sql = "SELECT * FROM address_book_entries WHERE email=\'" + email + "\'";
		try{
			Map<String, Object> resultSet = jdbcTemplate.queryForMap(sql);
			AddressBookEntry abe = convertMapToDTO(resultSet);
			addToCache(abe);
			return abe;
		} catch (EmptyResultDataAccessException e){
			System.out.print("User not found for Email: " + email + "\n ");
			AddressBookEntry abe = new AddressBookEntry();
			return abe;
		} 
	}

	@Transactional
	public String updateAddressBookEntry(AddressBookEntry abeOld, AddressBookEntry abeNew) {
		if(entryInCache(abeOld.getEmail()))	removeEntryFromCache(abeOld.getEmail());
		if(entryInCache(abeNew.getEmail())) return "\n Email already in use.";
		
		String sql = "UPDATE address_book_entries SET"
				+ " Name=\'" + abeNew.getName() + "\',"
				+ " Address=\'" + abeNew.getAddress() + "\',"
				+ " PhoneNumber=\'" + abeNew.getPhoneNum() + "\',"
				+ " Email=\'" + abeNew.getEmail() + "\',"
				+ " ZipCode=\'" + abeNew.getZipCode() + "\'"
				+ " WHERE"
				+ " Name=\'" + abeOld.getName() + "\' AND"
				+ " Address=\'" + abeOld.getAddress() + "\' AND"
				+ " PhoneNumber=\'" + abeOld.getPhoneNum() + "\' AND"
				+ " Email=\'" + abeOld.getEmail() + "\' AND"
				+ " ZipCode=\'" + abeOld.getZipCode() + "\'";
				
		jdbcTemplate.execute(sql);
		addToCache(abeNew);
		return "User updated successfully!";
	}

	@Transactional
	public void deleteAddressBookEntry(String email) {
		if(entryInCache(email)){	
			removeEntryFromCache(email);
			String sql = "DELETE FROM address_book_entries WHERE Email=\'" + email + "\'";
			jdbcTemplate.execute(sql);
		} else {
			System.out.print("User for email: " + email + " does not exist!");
		}
		
	}

	@Transactional
	public ArrayList<AddressBookEntry> getAllAddressBookEntries() {
		if(!addressBook.isEmpty()) return addressBook; 
		
		String sql = "SELECT * FROM address_book_entries";
		List<Map<String, Object>> resultSet = jdbcTemplate.queryForList(sql);
		
		resultSet.forEach(map -> addToCache(convertMapToDTO(map)));
		return addressBook;
	}
	
	private AddressBookEntry convertMapToDTO(Map<String, Object> resultSet){
		AddressBookEntry abe = new AddressBookEntry(
				resultSet.get("Name").toString(), 
				resultSet.get("Address").toString(), 
				resultSet.get("PhoneNumber").toString(), 
				resultSet.get("Email").toString(), 
				resultSet.get("ZipCode").toString());
				
		return abe;
	}
	
	private void addToCache(AddressBookEntry abe){
		addressBook.add(abe);
	}
		
	private boolean entryInCache(String email){
		return searchCache(email) > -1 ? true : false;	
	}
	
	private AddressBookEntry getCachedEntry(String email){
		return addressBook.get(searchCache(email)); 
	}
	
	private void removeEntryFromCache(String email){
		addressBook.remove(searchCache(email));
	}
	
	private int searchCache(String email){
		for (int i = 0; i < addressBook.size(); i++) {
			if(addressBook.get(i).getEmail().equals(email))	return i;
		}
		return -1;
	}
}
