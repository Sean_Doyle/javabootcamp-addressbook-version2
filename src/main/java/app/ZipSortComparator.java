package app;

import java.util.Comparator;

import app.dto.AddressBookEntry;

public interface ZipSortComparator {
	Comparator<AddressBookEntry> abeZipSort = new Comparator<AddressBookEntry>(){		
		public int compare(AddressBookEntry abe1, AddressBookEntry abe2){
			String abe1Zip = abe1.getZipCode().toLowerCase();
			String abe2Zip = abe2.getZipCode().toLowerCase();
			
			return abe1Zip.compareTo(abe2Zip);
		}
	};
}
