package app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

import app.dao.AddressBookEntryDAOImpl;
import app.dto.AddressBookEntry;

public class App implements NameSortComparator, ZipSortComparator{
	
	private BufferedReader br;
	private boolean isRunning = true;
	private AddressBookEntryDAOImpl abeDAO = new AddressBookEntryDAOImpl();
	
	public App(){
		br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	private void run(){
		clearScreen();
		do{			
			printMenuOptions();
			try{
				executeUserInput(Integer.parseInt(br.readLine()));
			} catch (NumberFormatException | IOException e){
				e.printStackTrace();
			}
		}while(isRunning);
		System.out.println("Application Closed.");
	}
	
	private void clearScreen(){
		for(int i = 0; i < 10; i ++){
			System.out.println("");
		}
	}
	
	private void printMenuOptions(){
		String msg = 
				" \n&=== Address Book App ===&"
				+ " \n"
				+ " 1. View Address Book Contents. \n"
				+ " 2. Add a New Address Book Entry. \n"
				+ " 3. Update an Address Book Entry. \n"
				+ " 4. Delete an Address Book Entry. \n"
				+ " \n"
				+ " 0. Exit Application. "
				+ " \n"
				+ " Please Select a Task(0-4): ";
		
		System.out.print(msg);
	}
	
	private void executeUserInput(int userInput){
		switch(userInput){
		case 1:
			printAddressBook();
			
			System.out.print("\nPress 'n' to sort by name or \nPress 'z' to sort by zip code or \nPress Enter to continue: ");
			String input = ""; 
			
			try{ 
				input = br.readLine().toLowerCase(); 
			} catch(IOException e){ e.printStackTrace(); }
		
			if(input.equals("n")) sortList(true); 		
			else if(input.equals("z")) sortList(false);
			
			break;
		case 2:
			addEntryToAddressBook();
			break;
		case 3:
			updateAddressBookEntry();
			break;
		case 4:
			removeAddressBookEntry();
			break;
		case 0:
			isRunning = false;
			break;
		default:
			System.out.println("Invalid Input! Input must be 0 -> 5");
			break;
		}
	}
	
	private void printAddressBook(){
		System.out.println("\n &=== CONTACTS ===&");
		
		ArrayList<AddressBookEntry> addressBook = abeDAO.getAllAddressBookEntries();
		for (AddressBookEntry abe : addressBook) {
			System.out.println(abe.formatedToString() + "\n");
		}
	}
	
	private void addEntryToAddressBook(){
		System.out.println("\n &=== NEW ENTRY ===&");
		try{
			System.out.print(" First Name?: "); String fName = br.readLine();
			System.out.print(" Surname?: "); String sName = br.readLine();
			System.out.print(" Address?: "); String address = br.readLine();
			System.out.print(" Phone Number?: "); String phoneNum = br.readLine();
			System.out.print(" Email?: "); String email = br.readLine();
			System.out.print(" Zip Code?: "); String zipCode = br.readLine();
			
			String name = sName + " " + fName;
			AddressBookEntry abe = new AddressBookEntry(name, address, phoneNum, email, zipCode);
			System.out.println(abeDAO.createAddressBookEntry(abe));
			
		} catch(IOException e){
			e.printStackTrace();
		}		
	}
	
	private void updateAddressBookEntry(){
		System.out.println("\n &=== UPDATE ENTRY ===&");
		try{
			System.out.print("Enter the email of the person you wish to edit: "); String emailToFind = br.readLine();
			AddressBookEntry abeOld = new AddressBookEntry(abeDAO.getAddressBookEntry(emailToFind));
			
			if(!abeOld.getEmail().equals("invalid_ABE")){
				System.out.print("Which detail would you like to edit?: "); 
				AddressBookEntry abeNew = editEntryDetail(abeOld, br.readLine().toLowerCase());
				abeDAO.updateAddressBookEntry(abeOld, abeNew);
			}
		} catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private void removeAddressBookEntry(){	
		printAddressBook();
		System.out.print("Please enter the email of user you wish to remove:");		
		
		try {
			String userInput = br.readLine();
			abeDAO.deleteAddressBookEntry(userInput);
		} catch (IOException e) { e.printStackTrace(); }
	}	
	
	private AddressBookEntry editEntryDetail(AddressBookEntry abe, String detail){
		abe = new AddressBookEntry(abe);
		try{
			if(detail.equals("name")){
				System.out.print("Please enter the new surname: "); String sname = br.readLine();
				System.out.print("Please enter the new firstname: "); String fname = br.readLine();
				abe.setName(sname + " " + fname);
			} else if(detail.equals("address")){
				System.out.print("Please enter the new address: "); String address = br.readLine();
				abe.setAddress(address);
			} else if(detail.equals("phone number")){
				System.out.print("Please enter the new phone number: "); String phoneNum = br.readLine();
				abe.setPhoneNum(phoneNum);
			} else if(detail.equals("email")){
				System.out.print("Please enter the new email: "); String email = br.readLine();
				abe.setEmail(email);
			} else if(detail.equals("zip code")){
				System.out.print("Please enter the new zip code: "); String zip = br.readLine();
				abe.setZipCode(zip);
			} else {
				
			}
		} catch(IOException e){ e.printStackTrace(); }
		return abe;
	}
	
	private void sortList(Boolean sortByName){
		ArrayList<AddressBookEntry> list = new ArrayList<AddressBookEntry>(abeDAO.getAllAddressBookEntries());
		
		if(sortByName) Collections.sort(list, abeNameSort);
		else Collections.sort(list, abeZipSort);
		
		System.out.println("\n &=== SORTED CONTACTS ===&");
		for (AddressBookEntry addressBookEntry : list) {
			System.out.println(addressBookEntry.formatedToString() + "\n");
		}
	}
	
	public static void main(String[] args){
		new App().run();
	}
}


