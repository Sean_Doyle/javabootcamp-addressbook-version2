package app.dto;

public class AddressBookEntry {

	private String
		name,
		address,
		phoneNum,
		email,
		zipCode;
	
	public AddressBookEntry(String name, String address, String phoneNum, String email, String zipCode){
		this.name = name;
		this.address = address;
		this.phoneNum = phoneNum;
		this.email = email;
		this.zipCode = zipCode;
	}
	
	public AddressBookEntry(AddressBookEntry abe) {
		this.name = abe.getName();
		this.address = abe.getAddress();
		this.phoneNum = abe.getPhoneNum();
		this.email = abe.getEmail();
		this.zipCode = abe.getZipCode();
	}
	
	public AddressBookEntry(){
		this.name = "invalid_ABE";
		this.address = "invalid_ABE";
		this.phoneNum = "invalid_ABE";
		this.email = "invalid_ABE";
		this.zipCode = "invalid_ABE";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	public String toString(){
		return name + "," + address + "," + phoneNum + "," + email + "," + zipCode + ",";
	}
	
	public String formatedToString(){
		return " Name: " + name +
				"\n Address: " + address +
				"\n Phone: "+ phoneNum +
				"\n Email: " + email +
				"\n Zip Code: " + zipCode;
	}
}
 
